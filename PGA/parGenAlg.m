function all = parGenAlg(popSize, matrix)
%vytvorenie subpopulacii
pop1 = genrpop(popSize, matrix);
pop2 = genrpop(popSize, matrix);
pop3 = genrpop(popSize, matrix);
pop4 = genrpop(popSize, matrix);
pop5 = genrpop(popSize, matrix);
pop6 = genrpop(popSize, matrix);
pop7 = genrpop(popSize, matrix);
pop8 = genrpop(popSize, matrix);
pop9 = genrpop(popSize, matrix);


%zlucim pre pouzitie genAlg a vraciam
all = [pop1; pop2; pop3; pop4; pop5; pop6; pop7; pop8; pop9];


%polia vstupujuce do grafu
x = int16.empty;
allResults = double.empty;

allR1 = double.empty;
allR2 = double.empty;
allR3 = double.empty;
allR4 = double.empty;
allR5 = double.empty;
allR6 = double.empty;
allR7 = double.empty;
allR8 = double.empty;
allR9 = double.empty;

%cykly/generacie jednotlivych populacii
for i = 1 : 100

%GA na kazdu subpopulaciu
pop1 = oneCycleGenAlg(pop1, popSize, matrix);
pop2 = oneCycleGenAlg(pop2, popSize, matrix);
pop3 = oneCycleGenAlg(pop3, popSize, matrix);
pop4 = oneCycleGenAlg(pop4, popSize, matrix);
pop5 = oneCycleGenAlg(pop5, popSize, matrix);
pop6 = oneCycleGenAlg(pop6, popSize, matrix);
pop7 = oneCycleGenAlg(pop7, popSize, matrix);
pop8 = oneCycleGenAlg(pop8, popSize, matrix);
pop9 = oneCycleGenAlg(pop9, popSize, matrix);

%periodicka migracia medzi subpopulaciami
if mod(i, 5) == 0
tmpPop = pop1;
pop1 = migration(pop1, pop2);
pop2 = migration(pop2, pop3);
pop3 = migration(pop3, pop4);
pop4 = migration(pop4, pop5);
pop5 = migration(pop5, pop6);
pop6 = migration(pop6, pop7);
pop7 = migration(pop7, pop8);
pop8 = migration(pop8, pop9);
pop9 = migration(pop9, tmpPop);
end

%vytiahnutie najlepsieho z kazdej subpopulacie
best1 = selbest(pop1, fitness(pop1), [1 0 0]);
best2 = selbest(pop2, fitness(pop2), [1 0 0]);
best3 = selbest(pop3, fitness(pop3), [1 0 0]);
best4 = selbest(pop4, fitness(pop4), [1 0 0]);
best5 = selbest(pop5, fitness(pop5), [1 0 0]);
best6 = selbest(pop6, fitness(pop6), [1 0 0]);
best7 = selbest(pop7, fitness(pop7), [1 0 0]);
best8 = selbest(pop8, fitness(pop8), [1 0 0]);
best9 = selbest(pop9, fitness(pop9), [1 0 0]);

%vyber najlepsieho z celej populacie
allBests = [best1; best2; best3; best4; best5; best6; best7; best8; best9];
result = selbest(allBests, fitness(allBests), [1 0 0]);

%priebezny zapis najlepsieho z populacie 
allResults(i) = fitness(result);
x(i) = i;

%priebezny zapis najlepsich zo subpopulacii
allR1(i) = fitness(best1);
allR2(i) = fitness(best2);
allR3(i) = fitness(best3);
allR4(i) = fitness(best4);
allR5(i) = fitness(best5);
allR6(i) = fitness(best6);
allR7(i) = fitness(best7);
allR8(i) = fitness(best8);
allR9(i) = fitness(best9);
end


plot(x, allR1, 'm', x, allR2, 'm', x, allR3, 'm', x, allR4, 'm', x, allR5, 'm', x, allR6, 'm', x, allR7, 'm', x, allR8, 'm', x, allR9, 'm', x, allResults, 'r');
hold on;
end