function newPop = oneCycleGenAlg(pop, popSize, matrix)
%instrukcie pre vyber najlepsich z populacie
matrixNumBest = [2 1 1];
numBest = getNumOfBest(matrixNumBest);
numOfOthers = popSize - numBest;
[~, numOfCoordinates] = size(pop);

%zaobstaranie velkosti populacie
if popSize <= numBest
   error("Prilis mala populacia");
end

%zaobstaranie poctu suradnic
if numOfCoordinates ~= 7
   error("Pocet suradnic iny ako 7");
end

y = fitness(pop);


%vyber najlepsich
bestPop = selbest(pop, y, matrixNumBest);

%vyber ostatnych
otherPop = selsus(pop, y, numOfOthers);

%krizenie a mutovanie ostatnych
otherPop = changeOtherPop(otherPop, matrix);

%zlucenie najlepsich a ostatnych
newPop = [bestPop;otherPop];
end