function otherPop = changeOtherPop(otherPop, matrix)
[~, columns] = size(matrix);
amp = ones(1, columns);
mutRate = 0.1;

%krizenie ostatnych
otherPop = crossov(otherPop, 2 ,0);

%globalne a lokalne mutovanie ostatnych
otherPop = mutx(otherPop, mutRate, matrix);
otherPop = muta(otherPop, mutRate, amp ,matrix);

end