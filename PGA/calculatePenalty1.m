function penalty = calculatePenalty1(pop)

penalty = 0;

if isNotInArea(pop) == true
   penalty =  10 ^ 10;
end


end

function result = isNotInArea(pop)

result = condition1(pop) || condition2(pop) || condition3(pop) || condition4(pop);

end

function bool = condition1(pop)
first = 2 * pop(1) ^ 2;
second = 3 * pop(2) ^ 4;
third = pop(3);
fourth = 4 * pop(4) ^ 2;
fifth = 5 * pop(5);

result = 127 - first - second - third - fourth - fifth;

bool = true;

if result >= 0
    bool = false;
end


end

function bool = condition2(pop)
first = 7 * pop(1);
second = 3 * pop(2);
third =  10 * pop(3) ^ 2;
fourth = pop(4);
fifth = pop(5);

result = 282 - first - second - third - fourth + fifth;

bool = true;

if result >= 0
    bool = false;
end


end

function bool = condition3(pop)
first = 23 * pop(1);
second = pop(2) ^ 2;
third = 6 * pop(6) ^ 2;
fourth = 8 * pop(7);

result = 196 - first - second - third + fourth;

bool = true;

if result >= 0
    bool = false;
end


end

function bool = condition4(pop)
first = 4 * pop(1) ^ 2;
second = pop(2) ^ 2;
third = 3 * pop(1) * pop(2);
fourth = 2 * pop(3) ^ 2;
fifth = 5 * pop(6);
sixth = 11 * pop(7);

result = - first - second + third - fourth - fifth + sixth;

bool = true;

if result >= 0
    bool = false;
end


end