function [Fit] = fitness(pop)

[popSize, ~] = size(pop);
Fit = [];

for i = 1 : popSize
   result = calculateResult(pop(i, : ));
   
   penalty = calculatePenalty3(pop(i, : ));
   
   result = result + penalty;
   
   Fit(1, i) = result;
end


end