function numBest = getNumOfBest(matrixNumBest)
[~, columns] = size(matrixNumBest);
numBest = 0;

for i = 1 : columns
numBest = numBest + matrixNumBest(i);
end
   
end