function destination = migration(destination, source)
%vyber najlepsieho z pop2
best = selbest(source, fitness(source), [1 0 0]);

[destinationSize, ~] = size(destination);

%zoradenie pop1 od najlepsieho po najhorsie
destination = selsort(destination, fitness(destination), destinationSize);

%najlepsi z pop2 vymeneny s najhorsim z pop1
destination = [destination(1 : destinationSize - 1, : ); best];
end