function result = calculateResult(pop)
first = (pop(1) - 10) ^ 2;
second = 5 * (pop(2) - 12) ^ 2;
third = pop(3) ^ 4;
fourth = 3 * (pop(4) - 11) ^ 2;
fifth = 10 * pop(5) ^ 6;
sixth = 7 * pop(6) ^ 2;
seventh = pop(7) ^ 4;
eighth = 4 * pop(6) * pop(7);
nineth = 10 * pop(6);
tenth = 8 * pop(7);

result = first + second + third + fourth + fifth + sixth + seventh - eighth - nineth - tenth;

end